#!/bin/bash
#
# start mavros node and its interface. 

source ros_environment.sh

# apriltag
roslaunch voxl_navigation apriltag.launch &

# voxblox 
roslaunch --wait voxblox_interface voxblox.launch &

# Mavros
roslaunch --wait mavros_interface run_mavros_with_interface.launch fcu_url:=udp://127.0.0.1:14550@:14550 gcs_url:=udp://@${QGC_IP} tgt_system:=${PX4_SYS_ID} 

