#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Range.h>
#include <std_msgs/Float64.h> // topic: /mavros/global_position/rel_alt
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2/LinearMath/Quaternion.h>

#include <mavros_msgs/Altitude.h> // topic: /mavros/altitude 
#include <mavros_msgs/State.h>

#define RATE 20
#define ALTITUDE_BUFFER_SIZE 10 

static ros::Subscriber state_sub; 
static ros::Subscriber local_pose_sub; 

static mavros_msgs::State current_state; 
static geometry_msgs::Twist current_pose;
static geometry_msgs::PoseStamped current_pose_stamped; 
static std::vector<sensor_msgs::Range> altitude_buffer; // Most recent first 

static ros::Subscriber range_down_near_sub; // vl53l1x subscriber 
static ros::Subscriber range_down_far_sub;  // teraranger subscriber

////////// Utils //////////

void store_altitude(sensor_msgs::Range range){
    altitude_buffer.insert(altitude_buffer.begin(),range); 
    if(altitude_buffer.size() > ALTITUDE_BUFFER_SIZE)
        altitude_buffer.pop_back(); 
}

/**
 * TODO: this altitude computation is simplistic, needs improvement 
 */
int compute_altitude(double &altitude){
    altitude = 0.0; 

    if(altitude_buffer.size() < ALTITUDE_BUFFER_SIZE)
        return -1; 

    // Naive altitude computation
    for(int i=0; i<altitude_buffer.size(); i++){
        altitude += altitude_buffer.at(i).range; 
    }altitude /= altitude_buffer.size();

    return 0; 
}

////////// Callbacks //////////

void state_cb(const mavros_msgs::State::ConstPtr& msg){
    current_state = *msg;
}

void pose_cb(const geometry_msgs::PoseStamped::ConstPtr &msg){

    current_pose_stamped = *msg; 

    // Express in NWU 
    current_pose.linear.x = msg->pose.position.x; 
    current_pose.linear.y = msg->pose.position.y; 
    current_pose.linear.z = msg->pose.position.z; 

    tf2::Quaternion q; 
    q.setW(msg->pose.orientation.w);
    q.setX(msg->pose.orientation.x);
    q.setY(msg->pose.orientation.y);
    q.setZ(msg->pose.orientation.z);

    tf2::Matrix3x3 m; m.setRotation(q);
    tf2Scalar roll, pitch, yaw;
    m.getRPY(roll, pitch, yaw);

    current_pose.angular.x = roll; 
    current_pose.angular.y = pitch; 
    current_pose.angular.z = yaw; 
}

void range_down_near_cb(const sensor_msgs::Range::ConstPtr &msg){
    store_altitude(*msg); 
}
void range_down_far_cb(const sensor_msgs::Range::ConstPtr &msg){
    store_altitude(*msg); 
}

////////// Main //////////

int main(int argc, char* argv[]){

    ros::init(argc, argv, "range_state_estimate");
    ros::NodeHandle nh;

    ROS_INFO("Range based state estimation started"); 

    ros::Rate rate(RATE);

    state_sub       = nh.subscribe<mavros_msgs::State>
                                        ("mavros/state", 10, state_cb);
    local_pose_sub  = nh.subscribe<geometry_msgs::PoseStamped>
                                        ("mavros/local_position/pose", 10, pose_cb);

    range_down_near_sub = nh.subscribe<sensor_msgs::Range>("/vl53l1x/down",10,range_down_near_cb); 
    range_down_far_sub = nh.subscribe<sensor_msgs::Range>("/teraranger",10,range_down_far_cb); 

    ros::Publisher altitude_pub = nh.advertise<std_msgs::Float64>("/mavros/global_position/rel_alt", 10); // TODO - maybe wrong topic 

    ros::Publisher pose_pub = nh.advertise<geometry_msgs::PoseStamped>("/mavros/vision_pose/pose", 10); // TODO - tell EKF2 to take into account

    while(ros::ok()){
        
        ros::spinOnce();

        static double altitude; 
        static std_msgs::Float64 alt_msg; 
        if(!compute_altitude(altitude)){
            alt_msg.data = altitude; 
            altitude_pub.publish(alt_msg);

            current_pose_stamped.pose.position.z = altitude;
            pose_pub.publish(current_pose_stamped);  
        }
        rate.sleep(); 
    }

    return 0; 
}
