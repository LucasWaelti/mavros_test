#!/bin/bash
#
# cleans the ros workspace
#
# Modal AI Inc. 2019
# author: james@modalai.com
# modifed: Lucas Waelti (July 2020)

rm -rf catkin_ws/build/
rm -rf catkin_ws/install/
rm -rf catkin_ws/devel/ # -> do not remove, eigen3 is installed there 

rm -rf catkin_ws/build_isolated/
rm -rf catkin_ws/install_isolated/
rm -rf catkin_ws/devel_isolated/
