#!/bin/bash
#
# Script to be executed when booting 
#
# Call it from /etc/modalai/docker-autorun.sh (called by docker-autorun.service on boot)
# Starts following components: 
#   docker: 
#       mavros (mavros)
#       mavros interface (mavros_interface)
#       apriltag (apriltag_ros)
#   yocto: 
#       stereo cameras (voxl_cam_ros)
#       snap_dfs (snap_dfs)
#       range finders (voxl_range_finder)

# load ros environment (see yocto's .bashrc)
. /etc/modalai/camera_env.sh
. /usr/bin/base_ros_env.sh
. /home/root/my_ros_env.sh
. /etc/modalai/modal_bash_tweaks.sh

function wait_for_master {
    while [ true ]; do 
        rostopic list | grep rosout > /dev/null
        if [ "$?" -eq 0 ]; then break; fi
    done
}

roscore &

wait_for_master
rosrun voxl_navigation clock_node & 
if [ "$?" -ne "0" ]; then
    echo ERROR: could not start clock_node 
else 
    echo Started clock_node
fi
sleep 1 

###################
## Camera server ##
###################
systemctl start voxl-camera-server 
if [ "$?" -ne "0" ]; then
    echo ERROR: could not start camera server 
else 
    echo Started camera server 
fi
sleep 1

############
## Docker ##
############
# Start the docker (mavros + interface, apriltag), do not use interactive mode 
/home/root/mavros_test/start_docker.sh run_mavros_apriltag.sh &
if [ "$?" -ne "0" ]; then
    echo ERROR: could not start docker 
else 
    echo Started docker container 
fi
sleep 1 

###########
## Yocto ##
###########

# Start the onboard sensors (stereo, snap_dfs, range finders)
#roslaunch --wait voxl_navigation voxl_sensing.launch  # DEPRECATED 

wait_for_master

# Start the vio monitor node
voxl-inspect-qvio -fqn | rosrun voxl_navigation vio_monitor_node &

# Start the camera info node
rosrun voxl_navigation camera_info_node & 
if [ "$?" -ne "0" ]; then
    echo ERROR: could not start camera_info_node 
else 
    echo Started camera_info_node
fi
sleep 1 

# Start the range finders
roslaunch --wait voxl_range_finder range_finder.launch & 
if [ "$?" -ne "0" ]; then
    echo ERROR: could not start range_finder 
else 
    echo Started range finders  
fi
sleep 1

# Start the cameras
roslaunch --wait voxl_mpa_to_ros voxl_mpa_to_ros.launch 
if [ "$?" -ne "0" ]; then
    echo ERROR: could not start voxl_mpa ros package 
else 
    echo Started voxl_mpa ros package  
fi

