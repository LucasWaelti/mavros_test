#!/bin/bash

# Get command line options 
NAV=true	# start navigation

while [[ $# -gt 0 ]]; do
	
	key="$1"

	case $key in 
	
		# disable navigation 
		-n|--no-nav) 
		NAV=false
		shift 
		;;
		
		# help
		-h|--help)
		echo "usage: ./start_cam_ros.sh [-n|--no-nav]"
		shift 
		exit
		;;
	
		# skip unknown args
		*)
		echo Warning: unknown arg \"$1\"
		shift 
		;;
		
	esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

cd /home/root/mavros_test

./setperfmode.sh && 

roslaunch --wait voxl_cam_ros stereo.launch width:=320 height:=240 &

roslaunch --wait snap_dfs dfs_node.launch & 

roslaunch --wait voxl_range_finder range_finder.launch &

if [[ $NAV = true ]]; then
	./start_docker.sh -i run_navigation.sh 
fi
