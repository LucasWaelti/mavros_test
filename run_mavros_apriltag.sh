#!/bin/bash
#
# start mavros node and its interface. 

source ros_environment.sh

# Mavros
roslaunch mavros_interface run_mavros_with_interface.launch \
    fcu_url:=udp://127.0.0.1:$UDP_PORT@:$UDP_PORT gcs_url:=udp://@${QGC_IP} tgt_system:=${PX4_SYS_ID} &

# apriltag
#roslaunch --wait voxl_navigation apriltag.launch 
roslaunch --wait /root/yoctohome/mavros_test/launch/apriltag.launch 