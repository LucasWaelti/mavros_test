#!/bin/bash

# Generate yaml code to declare 100 detectable tags. 
# examples: 
#   "{id: 04, size: *ts, name: 'tag04'},"
#   "{id: 15, size: *ts, name: 'tag15'},"

for i in {0..99}; do

    # add a padding 0 
    o=""
    if [ "$i" -lt "10" ]; then
        o="0"
    fi

    echo "{id: $i, size: *ts, name: 'tag$o$i'},"

done 