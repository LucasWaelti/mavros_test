#!/bin/bash
#
# Run ROS nodes at start up in the background within a container. 
#
# Start a container running the expanded image. 
# Execute one of the scripts of this directory by putting
# its name as an argument. It will be automatically
# executed within the generated container. 

# Base command would be (won't work because of symlink 
# on mavros_test directory)
# $ voxl-docker -i roskinetic-xenial:exp -w /root/yoctohome/
#   mavros_test/ -e "/bin/bash"

# Get command line options 
INTER="" 	# interactive mode 
SCRIPT=""	# script to launch in container 

while [[ $# -gt 0 ]]; do
	
	key="$1"

	case $key in 
	
		# enable interactive
		-i|--interactive) 
		INTER="-it"
		shift 
		;;
		
		# help
		-h|--help)
		echo "usage: ./start_docker.sh [-i|--interactive] [<SCRIPT>]"
		shift 
		exit
		;;
		
		# script
		*)
		SCRIPT=$1
		shift 
		;;
		
	esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

# Reproduce the result of the command: 
# $ voxl-docker -i roskinetic-xenial:exp
# but mount supplementary volumes to support the symbolic link 
docker run $INTER --rm --privileged --net=host -v /home/root:/root/yoctohome:rw -v /data/ros_storage/mavros_test:/data/ros_storage/mavros_test:rw -w /root/yoctohome/mavros_test/ roskinetic-xenial:exp /bin/bash $SCRIPT


