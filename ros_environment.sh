#!/bin/bash
#

source /opt/ros/kinetic/setup.bash
source catkin_ws/install_isolated/setup.bash
unset ROS_HOSTNAME

# configure ROS IPs here
export ROS_MASTER_IP=127.0.0.1
export ROS_IP=192.168.1.1 #android:192.168.xxx.238 / softap:192.168.8.1  / flying_arena:11.1.1.63
export QGC_IP=192.168.1.49 #android:192.168.xxx.227 / softap:192.168.8.49 / flying_arena:11.1.1.64
export ROS_MASTER_URI=http://${ROS_MASTER_IP}:11311/

# mavros needs to know what PX4's system id is
export PX4_SYS_ID=1

# set the udp port for mavros
export UDP_PORT=14551 # 14550 on older versions (system image <= 2.2.0)
