#!/bin/bash
#
# start mavros node and interface

source ros_environment.sh

#roslaunch mavros px4.launch fcu_url:=udp://127.0.0.1:14550@:14550 gcs_url:=udp://@${QGC_IP} tgt_system:=${PX4_SYS_ID} 

roslaunch mavros_interface run_mavros_with_interface.launch fcu_url:=udp://127.0.0.1:$UDP_PORT@:$UDP_PORT gcs_url:=udp://@${QGC_IP} tgt_system:=${PX4_SYS_ID} 
